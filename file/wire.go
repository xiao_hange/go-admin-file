//go:build wireinject

package main

import (
	"gitee.com/xiao_hange/go-admin-file/file/events/file"
	"gitee.com/xiao_hange/go-admin-file/file/grpc"
	"gitee.com/xiao_hange/go-admin-file/file/ioc"
	"gitee.com/xiao_hange/go-admin-file/file/ioc/config"
	"gitee.com/xiao_hange/go-admin-file/file/repository"
	"gitee.com/xiao_hange/go-admin-file/file/repository/dao"
	"gitee.com/xiao_hange/go-admin-file/file/service"
	"github.com/google/wire"
)

var thirdPartySet = wire.NewSet(
	ioc.InitTencentCos,
	ioc.InitKafka,
	ioc.InitRedis,
	ioc.InitDB,
	ioc.InitGrpcLogger,
	ioc.InitAppLogger,
	ioc.InitGormLogger,
)

var fileSvcProvider = wire.NewSet(
	service.NewFileService,
	repository.NewFileRepository,
	dao.NewFileDao,
)

func InitApp(mc *config.MysqlConfig, sc *config.ServerConfig, ka *config.KafkaConfig, ja *config.JaegerConfig, rc *config.RedisConfig) *App {
	wire.Build(
		fileSvcProvider,
		thirdPartySet,
		grpc.NewFileServiceServer,
		ioc.NewConsumers,
		file.NewInteractiveReadEventFileConsumer,
		file.NewKafkaProducer,
		ioc.NewSyncProducer,
		ioc.InitGrpcServer,
		wire.Struct(new(App), "*"),
	)
	return new(App)
}
