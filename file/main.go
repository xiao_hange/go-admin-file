package main

import (
	"context"
	"fmt"
	"gitee.com/xiao_hange/go-admin-file/file/ioc"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	serverName := "file_3"
	ac := ioc.InitOther(serverName)

	closeFunc := ioc.InitOTEL(ac.Jaeger)
	app := InitApp(ac.Mysql, ac.Server, ac.Kafka, ac.Jaeger, ac.Redis)
	for _, c := range app.consumers {
		err := c.Start()
		if err != nil {
			panic(err)
		}
	}
	go func() {
		err := app.server.Serve()
		if err != nil {
			panic("服务启动失败" + err.Error())
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	closeFunc(ctx)
	if err := app.server.Close(); err != nil {
		fmt.Println("服务关闭失败" + err.Error())
	}
}
