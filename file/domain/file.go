package domain

import (
	"io"
	"time"
)

type File struct {
	ID         int64     `json:"id"`
	Type       uint8     `json:"type"`
	OldName    string    `json:"old_name"`
	NewName    string    `json:"new_name"`
	FileType   string    `json:"file_type"`
	Size       string    `json:"size"`
	Image      string    `json:"image"`
	UpdateTime time.Time `json:"update_time" con:"date"`
	CreateTime time.Time `json:"create_time" con:"date"`
}

type UploadFlie struct {
	Val string
	Url string
}

type UploadFlieReq struct {
	OldName  string
	FileType string
	Size     string
	Reader   io.Reader
}

type Data struct {
	ID         int64
	Title      string
	Content    string
	UpdateTime time.Time `con:"date"`
	CreateTime time.Time `con:"date"`
}
