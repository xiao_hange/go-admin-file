package ioc

import (
	"gitee.com/xiao_hange/go-admin-file/file/ioc/config"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

// InitOther 初始化不在 wire_gen 里面的代码
func InitOther(serverName string) *config.AppConfig {
	if serverName == "" {
		panic("请输入服务名字")
	}
	InitViper()
	initPrometheus()
	ac := InitNacos(serverName)
	return ac
}
func initPrometheus() {
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe(":8083", nil)
	}()
}
