package ioc

import (
	"gitee.com/xiao_hange/go-admin-file/file/events/file"
	"gitee.com/xiao_hange/go-admin-file/file/ioc/config"
	"gitee.com/xiao_hange/go-admin-pkg/pkg/saramax"
	"github.com/IBM/sarama"
)

func InitKafka(cfg *config.KafkaConfig) sarama.Client {

	saramaCfg := sarama.NewConfig()
	saramaCfg.Producer.Return.Successes = true
	saramaCfg.Consumer.Return.Errors = true
	// TODO cfg.Addr需要改成字符串切片类型 []string
	client, err := sarama.NewClient([]string{cfg.Addr}, saramaCfg)
	if err != nil {
		panic(err)
	}
	return client
}

func NewSyncProducer(client sarama.Client) sarama.SyncProducer {
	res, err := sarama.NewSyncProducerFromClient(client)
	if err != nil {
		panic(err)
	}
	return res
}

func NewConsumers(c1 *file.InteractiveReadEventFileConsumer) []saramax.Consumer {
	return []saramax.Consumer{c1}
}
