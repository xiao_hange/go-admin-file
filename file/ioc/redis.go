package ioc

import (
	"gitee.com/xiao_hange/go-admin-file/file/ioc/config"
	"github.com/redis/go-redis/v9"
)

func InitRedis(cfg *config.RedisConfig) redis.Cmdable {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.Addr,
		Password: cfg.Pwd,
	})
	return redisClient
}
