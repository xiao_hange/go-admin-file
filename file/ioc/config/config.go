package config

type KafkaConfig struct {
	Addr string `yaml:"addr"`
}

type MysqlConfig struct {
	Addr string `yaml:"addr"`
}

type JaegerConfig struct {
	Addr string `yaml:"addr"`
}

type RedisConfig struct {
	Addr string `yaml:"addr"`
	Pwd  string `yaml:"pwd"`
}

type ServerConfig struct {
	Port       int    `yaml:"port"`
	Weight     int    `yaml:"weight"`
	EtcdAddr   string `yaml:"etcdAddr"`
	Label      string `yaml:"label"`
	ConsulAddr string `yaml:"consulAddr"`
	ServerName string `yaml:"serverName"`
}

type AppConfig struct {
	Kafka  *KafkaConfig  `yaml:"kafka"`
	Mysql  *MysqlConfig  `yaml:"mysql"`
	Jaeger *JaegerConfig `yaml:"jaeger"`
	Server *ServerConfig `yaml:"server"`
	Redis  *RedisConfig  `yaml:"redis"`
}
