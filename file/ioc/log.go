package ioc

import (
	"gitee.com/xiao_hange/go-admin-pkg/pkg/logger"
)

// InitHttpLogger 收集Http请求
func InitGrpcLogger() logger.GrpcLogger {
	return logger.NewGrpcLogger()
}

// InitGormLogger 收集Gorm操作
func InitGormLogger() logger.GormLogger {
	return logger.NewGormLogger()
}

// InitAppLogger 项目内log收集
func InitAppLogger() logger.AppLogger {
	return logger.SetGlobalLogger(logger.NewAppLogger())
}
