package ioc

import (
	"gitee.com/xiao_hange/go-admin-file/file/service/file"
	"gitee.com/xiao_hange/go-admin-file/file/service/file/tencent"
	"github.com/tencentyun/cos-go-sdk-v5"
	"net/http"
	"net/url"
	"os"
)

func InitTencentCos() file.Service {
	u, _ := url.Parse("")
	b := &cos.BaseURL{BucketURL: u}
	c := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  os.Getenv(""),
			SecretKey: os.Getenv(""),
		},
	})
	return tencent.NewService(c)
}
