package ioc

import (
	"github.com/spf13/viper"
)

func InitViper() {
	viper.SetConfigFile("config/dev.yaml")
	//viper.WatchConfig()
	err := viper.ReadInConfig()
	if err != nil {
		return
	}
}

//func InitViperRemote() {
//	err := viper.AddRemoteProvider("etcd3", "http://127.0.0.1:12379", "/webook")
//	if err != nil {
//		panic(err)
//	}
//	viper.SetConfigType("yaml")
//	//err = viper.WatchRemoteConfig()
//	if err != nil {
//		panic(err)
//	}
//	err = viper.ReadRemoteConfig()
//	if err != nil {
//		panic(err)
//	}
//}
