package mail

import (
	"fmt"
	"gopkg.in/gomail.v2"
)

func initMail() (*gomail.Dialer, *gomail.Message) {
	d := gomail.NewDialer("smtp.qq.com", 465, "961685762@qq.com", "lxkjhpakeudgbfjh")
	// 创建邮件
	m := gomail.NewMessage()
	m.SetHeader("From", "961685762@qq.com")
	m.SetHeader("Subject", "测试邮件")
	m.SetBody("text/plain", "测试邮件用于接收大表格数据")
	return d, m
}
func SendMail(to, attachmentPath string) error {
	d, m := initMail()
	m.SetHeader("To", to)
	if attachmentPath != "" {
		m.Attach(attachmentPath)
	}
	return d.DialAndSend(m)
}

func SendMailForCos(to, downloadLink string) error {
	d, m := initMail()
	m.SetHeader("To", to)
	if downloadLink != "" {
		// 将下载链接添加到邮件正文
		body := fmt.Sprintf("测试邮件用于接收大表格数据。点击以下链接下载附件：<a href='%s'>点击下载</a>", downloadLink)
		m.SetBody("text/html", body)
	}
	// 发送邮件
	return d.DialAndSend(m)
}
