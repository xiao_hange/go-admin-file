package tencent

import (
	"context"
	"github.com/tencentyun/cos-go-sdk-v5"
	"io"
)

type Service struct {
	client *cos.Client
}

func NewService(client *cos.Client) *Service {
	return &Service{
		client: client,
	}
}

func (s *Service) UploadImage(ctx context.Context, name string, reader io.Reader) error {
	_, err := s.client.Object.Put(ctx, name, reader, nil)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) DeleteImage(ctx context.Context, key string) error {
	_, err := s.client.Object.Delete(ctx, key)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) UploadCsv(ctx context.Context, name string, reader io.Reader) error {

	opt := &cos.ObjectPutOptions{
		ObjectPutHeaderOptions: &cos.ObjectPutHeaderOptions{
			ContentType: "text/csv", // 设置文件类型
		},
	}

	_, err := s.client.Object.Put(ctx, name, reader, opt)
	if err != nil {
		return err
	}
	return nil
}
