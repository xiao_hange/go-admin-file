package file

import (
	"context"
	"io"
)

type Service interface {
	UploadImage(ctx context.Context, name string, reader io.Reader) error
	UploadCsv(ctx context.Context, name string, reader io.Reader) error
	DeleteImage(ctx context.Context, key string) error
}
